package org.camunda.bpm.getstarted.locationrequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ProcessRequestDelegate implements JavaDelegate {

  private final static Logger LOGGER = Logger.getLogger("LOCATION-REQUEST");

  public void execute(DelegateExecution execution) throws Exception {
    LOGGER.info("Processing request by '" + execution.getVariable("Address") + "'...");

    String address = execution.getVariable("Address").toString();
    CloseableHttpClient httpClient= HttpClientBuilder.create().build();
    HttpPost httpPost = new HttpPost("http://localhost:8070/proxy");

    // Request parameters and other properties.
    List<NameValuePair> params = new ArrayList<NameValuePair>();
    params.add(new BasicNameValuePair("address", String.join("", "[\" ", address, " \"]")));
    try {
      httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    /*
     * Execute the HTTP Request
     */
    try {
      HttpResponse response = httpClient.execute(httpPost);
      HttpEntity respEntity = response.getEntity();

      if (respEntity != null) {
        String content = EntityUtils.toString(respEntity);
        LOGGER.info("Response from proxy " + content);
        execution.setVariable("Location", content);
      }
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      httpClient.close();
    }
  }
}
