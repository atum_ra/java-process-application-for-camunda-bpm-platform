package org.camunda.bpm.getstarted.locationrequest;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

// @ProcessApplication("Loan Approval App")
@ProcessApplication("Address Request App")
public class AddressRequestApplication extends ServletProcessApplication {
  // empty implementation
}
